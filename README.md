
#Backend de gestion des Virades du Creps 2016 organisées par le PAC.
[![Build Status][travis-badge]][travis-badge-url]

http://www.coursesduconfluent.fr/accueil-virades

Sert à compter les tours par participant/dossard, déterminer le rythme et la vitesse moyenne, le temps par tour, etc...
