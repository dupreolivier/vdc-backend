/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dao;

import fr.coursesduconfluent.model.Runner;
import fr.coursesduconfluent.model.Team;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class RunnerRepositoryImpl implements RunnerRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunnerRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Set<Runner> findAny(final String request) {
        // Get runners for all members
        final Criteria bib = Criteria.where(Runner.BIB).regex(request, "i");
        final Criteria firstName = Criteria.where(Runner.FIRST_NAME).regex(request, "i");
        final Criteria lastName = Criteria.where(Runner.LAST_NAME).regex(request, "i");
        final Criteria email = Criteria.where(Runner.EMAIL).regex(request, "i");
        final Criteria birthDate = Criteria.where(Runner.BIRTH_DATE).regex(request, "i");
        final Criteria category = Criteria.where(Runner.CATEGORY).regex(request, "i");
        final Criteria allCriterias = new Criteria().orOperator(bib, firstName, lastName, email, birthDate, category);

        final Query queryRunners = new Query(allCriterias);
        LOGGER.trace("Querying MongoBD to look for Runners matching {}, using {}", request, queryRunners);

        return new HashSet<>(mongoTemplate.find(queryRunners, Runner.class));
    }

    @Override
    public long count() {
        return ((List) findAll()).size();
    }

    @Override
    public void delete(final String bib) {
        final Query query = new Query(Criteria.where(Runner.BIB).is(bib));

        mongoTemplate.findAndRemove(query, Runner.class);
    }

    @Override
    public void delete(final Runner runner) {
        delete(runner.getBib());
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(Runner.class);
    }

    @Override
    public boolean exists(final String runnerBib) {
        final Query query = new Query(Criteria.where(Runner.BIB).is(runnerBib));

        return mongoTemplate.exists(query, Runner.class);
    }

    @Override
    public Iterable<Runner> findAll() {
        return findRunners(new Query());
    }

    @Override
    public Iterable<Runner> findAll(final Iterable<String> runnersBibsList) {
        final Query query = new Query(Criteria.where(Runner.BIB).in(runnersBibsList));

        return findRunners(query);
    }

    @Override
    public Runner findOne(final String bib) {
        return findByBib(bib);
    }

    private SortedSet<Runner> findRunners(final Query query) {
        LOGGER.trace("Looking for runners matching query {}", query);

        final SortedSet<Runner> runners = new TreeSet<>();

        runners.addAll(mongoTemplate.find(query, Runner.class));

        LOGGER.trace("{} runners retrieved from the database", runners.size());

        return runners;
    }

    @Override
    public <S extends Runner> S save(final S runner) {
        mongoTemplate.save(runner);

        return runner;
    }

    @Override
    public <S extends Runner> Iterable<S> save(final Iterable<S> runnerList) {
        final Set<Runner> savedRunnerList = new HashSet<>();
        for (Runner runner : runnerList) {
            savedRunnerList.add(save(runner));
        }

        return (Iterable<S>) savedRunnerList;
    }

    @Override
    public void delete(final Iterable<? extends Runner> runnerBibsSet) {
        final Set<String> toBeDeletedSet = new HashSet<>();
        for (Runner runner : runnerBibsSet) {
            toBeDeletedSet.add(runner.getBib());
        }

        delete(toBeDeletedSet);
    }

    @Override
    public Runner findByBib(final String bib) {
        final Query query = new Query(Criteria.where(Runner.BIB).is(bib));

        return mongoTemplate.findOne(query, Runner.class);
    }

    @Override
    public Set<Runner> findByEmail(final String email) {
        final Query query = new Query(Criteria.where(Runner.EMAIL).is(email));

        return findRunners(query);
    }

    @Override
    public Set<Runner> findByLastName(final String lastName) {
        final Query query = new Query(Criteria.where(Runner.LAST_NAME).is(lastName));

        return findRunners(query);
    }

    @Override
    public Set<Runner> findByFirstName(final String firstName) {
        final Query query = new Query(Criteria.where(Runner.FIRST_NAME).is(firstName));

        return findRunners(query);
    }

    @Override
    public Team findTeam(final String runnerBib) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.s
    }

    @Override
    public void delete(final Set<String> runnerBibs) {
        final Query query = new Query(Criteria.where(Runner.BIB).in(runnerBibs));

        mongoTemplate.findAndRemove(query, Runner.class);
    }
}
