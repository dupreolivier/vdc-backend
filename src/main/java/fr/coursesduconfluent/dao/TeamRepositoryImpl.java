/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dao;

import fr.coursesduconfluent.model.Team;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class TeamRepositoryImpl implements TeamRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Iterable<Team> findAll() {
        return new TreeSet<>(mongoTemplate.findAll(Team.class));
    }

    @Override
    public long count() {
        return ((Set) findAll()).size();
    }

    @Override
    public void delete(final String teamName) {
        final Query query = new Query(Criteria.where(Team.NAME).is(teamName));

        mongoTemplate.findAndRemove(query, Team.class);
    }

    @Override
    public void delete(final Team team) {
        this.deleteTeam(team.getName());
    }

    @Override
    public Team deleteTeam(final String name) {
        final Query query = new Query(Criteria.where(Team.NAME).is(name));

        return mongoTemplate.findAndRemove(query, Team.class);

    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(Team.class);
    }

    @Override
    public boolean exists(final String teamName) {
        final Query query = new Query(Criteria.where(Team.NAME).is(teamName));

        return mongoTemplate.exists(query, Team.class);
    }

    @Override
    public Iterable<Team> findAll(final Iterable<String> teamNamesList) {
        final Query query = new Query(Criteria.where(Team.NAME).in(teamNamesList));

        return mongoTemplate.find(query, Team.class);
    }

    @Override
    public Team findOne(final String teamName) {
        final Query query = new Query(Criteria.where(Team.NAME).is(teamName));

        return mongoTemplate.findOne(query, Team.class);
    }

    @Override
    public <S extends Team> S save(final S team) {
        mongoTemplate.save(team);

        return team;
    }

    @Override
    public <S extends Team> Iterable<S> save(final Iterable<S> teamSet) {
        final Set<Team> savedTeamsSet = new HashSet<>();
        for (Team team : teamSet) {
            savedTeamsSet.add(save(team));
        }

        return (Iterable<S>) savedTeamsSet;
    }

    @Override
    public void delete(final Iterable<? extends Team> teamSet) {
        final Set<String> toBeDeletedList = new HashSet<>();
        for (Team team : teamSet) {
            toBeDeletedList.add(team.getName());
        }

        final Query query = new Query(Criteria.where(Team.NAME).in(toBeDeletedList));

        mongoTemplate.findAndRemove(query, Team.class);
    }
}
