/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dao;

import fr.coursesduconfluent.model.Loop;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class LoopRepositoryImpl implements LoopRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    private final List<Loop> loops = new ArrayList<>();
    private final Sort sortDescNumber = new Sort(Sort.Direction.DESC, Loop.NUMBER);

    @Override
    public SortedSet<Loop> findAllForRunner(final String bib) {
        final Query query = new Query(Criteria.where(Loop.RUNNER_BIB).is(bib)).with(sortDescNumber);

        return findLoops(query);
    }

    @Override
    public SortedSet<Loop> findAllForTeam(final String teamBib) {
        final Query query = new Query(Criteria.where(Loop.RUNNER_BIB).is(teamBib)).with(sortDescNumber);


        return findLoops(query);
    }

    @Override
    public Iterable<Loop> findAll() {
        return findLoops(new Query());
    }

    @Override
    public Iterable<Loop> findAll(Iterable<Integer> loopIdList) {
        final Query query = new Query(Criteria.where(Loop.ID).in(loopIdList));

        return findLoops(query);
    }

    @Override
    public Loop findOne(final Integer id) {
        final Query query = new Query(Criteria.where(Loop.ID).is(id));

        return mongoTemplate.findOne(query, Loop.class);
    }

    private SortedSet<Loop> findLoops(final Query query) {
        return new TreeSet<>(mongoTemplate.find(query, Loop.class));
    }

    @Override
    public long count() {
        return ((Set) findAll()).size();
    }

    @Override
    public void delete(Integer id) {
        final Query query = new Query(Criteria.where(Loop.ID).is(id));

        mongoTemplate.findAndRemove(query, Loop.class);
    }

    @Override
    public void delete(Loop loop) {
        Query query = new Query(Criteria.where(Loop.ID).is(loop.getId()));

        mongoTemplate.findAndRemove(query, Loop.class);
    }

    @Override
    public void delete(Iterable<? extends Loop> loopList) {
        final List<String> toBeDeletedList = new ArrayList<>();
        for (Loop loop : loopList) {
            toBeDeletedList.add(loop.getId());
        }

        final Query query = new Query(Criteria.where(Loop.ID).in(toBeDeletedList));

        mongoTemplate.findAndRemove(query, Loop.class);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(Loop.class);
    }

    @Override
    public boolean exists(Integer id) {
        final Query query = new Query(Criteria.where(Loop.ID).is(id));

        return mongoTemplate.exists(query, Loop.class);
    }

    @Override
    public <S extends Loop> S save(S loop) {
        mongoTemplate.save(loop);

        return loop;
    }

    @Override
    public <S extends Loop> Iterable<S> save(Iterable<S> loopList) {
        final List<Loop> savedLoopList = new ArrayList<>();
        for (Loop loop : loopList) {
            savedLoopList.add(save(loop));
        }

        return (Iterable<S>) savedLoopList;
    }
}
