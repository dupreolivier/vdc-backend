/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dao;

import fr.coursesduconfluent.model.Run;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class RunRepositoryImpl implements RunRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public <S extends Run> S save(final S run) {
        LOGGER.info("Saving run {}", run);

        mongoTemplate.save(run);

        return run;
    }

    @Override
    public Run findOne(final String title) {
        final Query query = new Query(Criteria.where(Run.TITLE).is(title));

        return mongoTemplate.findOne(query, Run.class);
    }

    @Override
    public Iterable<Run> findAll() {
        final Query query = new Query();

        return new TreeSet<>(mongoTemplate.findAll(Run.class));
    }

    @Override
    public void delete(final String title) {
        final Query query = new Query(Criteria.where(Run.TITLE).is(title));

        mongoTemplate.findAndRemove(query, Run.class);
    }

    @Override
    public long count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Run entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Iterable<? extends Run> entities) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAll() {
        mongoTemplate.dropCollection(Run.class);
    }

    @Override
    public boolean exists(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Run> findAll(Iterable<String> ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <S extends Run> Iterable<S> save(Iterable<S> entities) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
