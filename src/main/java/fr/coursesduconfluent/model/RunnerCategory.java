/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see http://www.athle.fr/asp.net/main.html/html.aspx?htmlid=25
 */
public enum RunnerCategory implements Category{
    M5F("Masters 5", 1936),
    M5H("Masters 5", 1936),
    M4F("Masters 4", 1946),
    M4H("Masters 4", 1946),
    M3F("Masters 3", 1956),
    M3H("Masters 3", 1956),
    M2F("Masters 2", 1966),
    M2H("Masters 2", 1966),
    M1F("Masters 1", 1976),
    M1H("Masters 1", 1976),
    SEF("Seniors", 1993),
    SEH("Seniors", 1993),
    ESF("Espoirs", 1996),
    ESH("Espoirs", 1996),
    //    JU("Juniors", 1998),
    //    CA("Cadets", 2000),
    //    MI("Minimes", 2002),
    //    BE("Benjamins", 2004),
    //    PO("Poussins", 2006),
    //    EA("École d'Athlétisme", 2009),
    //    BB("Baby Athlé", 2010)
    ;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RunnerCategory.class);
    
    private String category;
    private int lastBirthYear;

    /**
     * Returns the category which name starts with the given one...
     *
     * @param name the category name
     *
     * @return the category.
     */
    public static RunnerCategory startsWith(final String name) {
        for (RunnerCategory category : values()) {
            LOGGER.trace("Is {} start of category's name {}?", name, category.name());
            
            if (name.startsWith(category.name())) {
                LOGGER.trace("Yes...");
                return category;
            }
        }
        
        return null;
    }
    
    private RunnerCategory(final String categorie, final int lastBirthYear) {
        this.category = categorie;
        this.lastBirthYear = lastBirthYear;
    }
    
    @Override
    public String getCategory() {
        return category;
    }
    
    public void setCategory(final String categorie) {
        this.category = categorie;
    }
    
    public int getLastBirthYear() {
        return lastBirthYear;
    }
    
    public void setLastBirthYear(final int lastBirthYear) {
        this.lastBirthYear = lastBirthYear;
    }
    
    @Override
    public String toString() {
        return "Category{" + "category=" + category + ", lastBirthYear=" + lastBirthYear + '}';
    }
    
}
