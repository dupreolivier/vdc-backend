/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see http://www.athle.fr/asp.net/main.html/html.aspx?htmlid=25
 */
public enum TeamCategory implements Category {
    MIXTE("Mixte"),
    FEMME("Femme"),
    JEUNE("Jeune"),
    HOMME("Homme");

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamCategory.class);

    private String category;

    /**
     * Returns the category which name starts with the given one...
     *
     * @param name the category name
     *
     * @return the category.
     */
    public static TeamCategory startsWith(final String name) {
        for (TeamCategory category : values()) {
            LOGGER.trace("Is {} start of category's name {}?", name, category.name());

            if (name.startsWith(category.name())) {
                LOGGER.trace("Yes...");
                return category;
            }
        }

        return null;
    }

    private TeamCategory(final String categorie) {
        this.category = categorie;
    }

    @Override
    public String getCategory() {
        return category;
    }

    public void setCategory(final String categorie) {
        this.category = categorie;
    }

    @Override
    public String toString() {
        return "TeamCategory{" + "category=" + category + '}';
    }
}
