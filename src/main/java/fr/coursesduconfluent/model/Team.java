/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.model;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Team extends Runner {

    private static final Logger LOGGER = LoggerFactory.getLogger(Team.class);

    public static final String NAME = "name";

    private String name;
    private TeamCategory teamCategory;
    private SortedSet<String> runnerNamesSet = new TreeSet<>();

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void addRunners(final String... runnerNamesSet) {
        this.runnerNamesSet.addAll(Arrays.asList(runnerNamesSet));
    }

    public void removeRunners(final String... runnerNamesSet) {
        this.runnerNamesSet.removeAll(Arrays.asList(runnerNamesSet));
    }

    public SortedSet<String> getRunnerNamesSet() {
        return runnerNamesSet;
    }

    public void setRunnerNamesSet(SortedSet<String> runnerNamesSet) {
        this.runnerNamesSet = runnerNamesSet;
    }

    public TeamCategory getTeamCategory() {
        return teamCategory;
    }

    public void setTeamCategory(TeamCategory teamCategory) {
        this.teamCategory = teamCategory;
    }

    @Override
    public String toString() {
        return "Team{" + "name=" + name + ", teamCategory=" + teamCategory + ", runnerNamesSet=" + runnerNamesSet + '}';
    }
}