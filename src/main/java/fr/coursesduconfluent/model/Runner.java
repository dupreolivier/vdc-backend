/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.model;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

public class Runner implements Comparable<Runner> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Runner.class);

    public static final String BIB = "bib";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String IS_WOMAN = "isWoman";
    public static final String BIRTH_DATE = "birthDate";
    public static final String CATEGORY = "category";
    public static final String WOMAN = "Femme";

    @Id
    private String bib;
    private String lastName, firstName, email, team;
    private boolean isWoman;
    private Date birthDate;
    private RunnerCategory category;

    @Override
    public int compareTo(final Runner runner) {
        try {
            final Integer thisBib = Integer.parseInt(bib);
            final Integer runnerBib = Integer.parseInt(runner.bib);
            
            return thisBib.compareTo(runnerBib);
        } catch (Exception e) {
            return bib.compareTo(runner.bib);
        }
    }

    public String getBib() {
        return bib;
    }

    public void setBib(final String bib) {
        this.bib = bib;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public boolean isIsWoman() {
        return isWoman;
    }

    public void setIsWoman(boolean isWoman) {
        this.isWoman = isWoman;
    }

    public RunnerCategory getCategory() {
        return category;
    }

    public void setCategory(RunnerCategory category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Runner{" + "bib=" + bib + ", lastName=" + lastName + ", firstName=" + firstName + ", email=" + email + ", isWoman=" + isWoman + ", team=" + team + ", birthDate=" + birthDate + '}';
    }
}
