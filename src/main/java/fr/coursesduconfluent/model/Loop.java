/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.model;

import java.time.LocalDateTime;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

public class Loop implements Comparable<Loop> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Loop.class);

    public static final String ID = "id";
    public static final String NUMBER = "number";
    public static final String RUNNER_BIB = "runnerBib";

    @Id
    private String id;

    private String runnerBib;

    private Integer number = 0;
    private LocalDateTime time;
    private int duration;

    private float speed, rythm;

    @Override
    public int compareTo(final Loop loop) {
        return this.getNumber().compareTo(loop.getNumber());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Loop other = (Loop) obj;
        if (!Objects.equals(this.runnerBib, other.runnerBib)) {
            return false;
        }
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        return true;
    }

    public float getRythm() {
        return rythm;
    }

    public void setRythm(float rythm) {
        this.rythm = rythm;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.runnerBib);
        hash = 47 * hash + Objects.hashCode(this.number);
        return hash;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(final int duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(final Integer number) {
        this.number = number;
    }

    public String getRunnerBib() {
        return runnerBib;
    }

    public void setRunnerBib(final String runnerBib) {
        this.runnerBib = runnerBib;
    }
}
