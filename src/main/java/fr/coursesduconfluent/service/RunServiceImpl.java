/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.dao.RunRepository;
import fr.coursesduconfluent.dto.RunDto;
import fr.coursesduconfluent.model.Run;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Run management service implementation.
 */
@Service
public class RunServiceImpl implements RunService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunServiceImpl.class);

    // TODO Change this. This is NOT RestFUL. Run must be provided to all calls... Or given an id?
    private Run run;

    @Autowired
    private final RunRepository runRepository;

    @Autowired
    public RunServiceImpl(final RunRepository runRepository) {
        this.runRepository = runRepository;
    }

    @Override
    public RunDto start(final RunDto givenRun) {
        run = runRepository.findOne(givenRun.getTitle());
        LOGGER.info("Run retrieved from DB: {}", run);

        if (run == null || run.getEnd() != null) {
            if (run != null) {
                runRepository.delete(run.getTitle());
                LOGGER.info("Deleted previous run");
            }

            run = new Run();
            run.setTitle(givenRun.getTitle());
            run.setStart(LocalDateTime.now());
            run.setDate(LocalDate.now());
            run.setLength(givenRun.getLength());
            run.setLocation(givenRun.getLocation());

            run = runRepository.save(run);

            LOGGER.info("Run saved: {}", run);
        }

        final RunDto runDto = new RunDto(run);

        return runDto;
    }

    @Override
    public RunDto end(final RunDto givenRun) {
        run = runRepository.findOne(givenRun.getTitle());

        run.setEnd(LocalDateTime.now());

        run = runRepository.save(run);

        final RunDto runDto = new RunDto(run);

        return runDto;
    }

    @Override
    public SortedSet<Run> listAll() {
        return (SortedSet<Run>) runRepository.findAll();
    }

    @Override
    public void deleteAll() {
        runRepository.deleteAll();
    }

    @Override
    public int getDuration() {
        return (int) (run != null ? run.getDuration() : Duration.ZERO).getSeconds();
    }

    @Override
    public LocalDateTime getStart() {
        return run != null ? run.getStart() : null;
    }

    @Override
    public int getLength() {
        return run != null ? run.getLength() : 0;
    }
}
