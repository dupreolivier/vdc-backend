/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.dto.LoopByRunner;
import fr.coursesduconfluent.dto.LoopsDone;
import fr.coursesduconfluent.model.Loop;
import java.util.List;
import java.util.SortedSet;

/**
 * Manage loops during the run.
 */
public interface LoopService {

    SortedSet<Loop> byRunner(final String bib);

    SortedSet<Loop> byTeam(final String name);

    List<LoopsDone> byCategory();

    void deleteAll();

    void deleteLast(final String bib);

    SortedSet<LoopByRunner> listAll();

    void loop(final String bib);

}
