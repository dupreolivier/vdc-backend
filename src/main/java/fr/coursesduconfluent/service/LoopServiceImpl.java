/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.dao.LoopRepository;
import fr.coursesduconfluent.dto.LoopByRunner;
import fr.coursesduconfluent.dto.LoopsDone;
import fr.coursesduconfluent.dto.RunnerDto;
import fr.coursesduconfluent.model.Loop;
import fr.coursesduconfluent.model.Runner;
import fr.coursesduconfluent.model.RunnerCategory;
import fr.coursesduconfluent.model.Team;
import fr.coursesduconfluent.model.TeamCategory;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Loops management service implementation.
 */
@Service
public class LoopServiceImpl implements LoopService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopServiceImpl.class);

    @Autowired
    private final LoopRepository loopRepository;

    @Autowired
    private final RunService runService;
    @Autowired
    private final RunnerService runnerService;
    @Autowired
    private final TeamService teamService;

    @Autowired
    public LoopServiceImpl(final LoopRepository loopRepository, final RunService runService, final RunnerService runnerService, final TeamService teamService) {
        this.loopRepository = loopRepository;
        this.runService = runService;
        this.runnerService = runnerService;
        this.teamService = teamService;
    }

    @Override
    public SortedSet<LoopByRunner> listAll() {
        final Set<Runner> runners = runnerService.listAll();
        final SortedSet<LoopByRunner> loopsByRunner = new TreeSet<>();

        runners.stream().forEach((runner) -> {
            LOGGER.trace("Looking for loops done by runner {}", runner);
            final RunnerDto runnerDto = new RunnerDto(runner);
            final SortedSet<Loop> runnerLoops = loopRepository.findAllForRunner(runner.getBib());
            updateRunner(runnerDto, runnerLoops);
            LOGGER.trace("{} loops done by runner {} so far", runnerLoops.size(), runner.getBib());

            loopsByRunner.add(new LoopByRunner(runnerDto, runnerLoops));
        });

        return loopsByRunner;
    }

    @Override
    public void deleteLast(final String bib) {
        final SortedSet<Loop> runnerLoops = byRunner(bib);
        loopRepository.delete(runnerLoops.first());
    }

    @Override
    public void deleteAll() {
        loopRepository.deleteAll();
    }

    @Override
    public void loop(final String bib) {
        LOGGER.trace("Adding loop for runner {}", bib);

        final LocalDateTime now = LocalDateTime.now();

        final SortedSet<Loop> runnerLoops = byRunner(bib);
        LOGGER.trace("Runner {} has already done {} loops", bib, runnerLoops.size());

        final Loop loop = new Loop();
        loop.setRunnerBib(bib);

        LocalDateTime start;

        if (runnerLoops.isEmpty()) {
            start = runService.getStart();

            loop.setNumber(0);
        } else {
            final Loop lastLoop = runnerLoops.last();
            start = lastLoop.getTime();

            loop.setNumber(lastLoop.getNumber() + 1);
        }

        loop.setTime(now);

        LOGGER.trace("Loop.start {}, loop.end {}", start, now);
        final Duration duration = Duration.between(start, now);

        if (duration.minusMinutes(1L).isNegative()) {
            LOGGER.warn("NOT saving the loop for runner {}, which last only {}s", bib, duration.getSeconds());
        } else {
            loop.setDuration((int) duration.getSeconds());

            int length = runService.getLength();

            loop.setSpeed(computeSpeed(length, loop.getDuration()));
            loop.setRythm(computeRythm(length, loop.getDuration()));

            LOGGER.debug("Saving loop #{} for runner {}, which last {}", loop.getNumber(), bib, loop.getDuration());
            loopRepository.save(loop);
        }
    }

    @Override
    public SortedSet<Loop> byTeam(final String name) {
        return loopRepository.findAllForTeam(name);
    }

    @Override
    public SortedSet<Loop> byRunner(final String bib) {
        return loopRepository.findAllForRunner(bib);
    }

    @Override
    public List<LoopsDone> byCategory() {
        final List<LoopsDone> runnersByCategory = runnersByCategory();
        LOGGER.debug("{} LoopsDone retrieved for runners", runnersByCategory.size());

        final List<LoopsDone> teamsByCategory = teamsByCategory();
        LOGGER.debug("{} LoopsDone retrieved for teams", teamsByCategory.size());

        final List<LoopsDone> byCategory = new ArrayList<>(runnersByCategory);
        byCategory.addAll(runnersByCategory);
        byCategory.addAll(teamsByCategory);

        return byCategory;
    }

    private List<LoopsDone> runnersByCategory() {
        final Map<RunnerCategory, Set<Runner>> runnersByCategories = runnerService.listByCategory();

        final List<LoopsDone> runnersByCategory = new ArrayList<>();
        runnersByCategories.entrySet().stream().forEach((entry) -> {
            final SortedSet<LoopByRunner> loopsByRunner = new TreeSet<>();

            entry.getValue().stream().forEach((runner) -> {
                LOGGER.trace("Looking for loops done by runner {}", runner);
                final RunnerDto runnerDto = new RunnerDto(runner);
                final SortedSet<Loop> runnerLoops = loopRepository.findAllForRunner(runner.getBib());
                updateRunner(runnerDto, runnerLoops);
                LOGGER.trace("{} loops done by runner {} so far", runnerLoops.size(), runner.getBib());

                loopsByRunner.add(new LoopByRunner(runnerDto, runnerLoops));
            });

            runnersByCategory.add(new LoopsDone(entry.getKey(), loopsByRunner));
        });

        return runnersByCategory;
    }

    private List<LoopsDone> teamsByCategory() {
        final Map<TeamCategory, Set<Team>> teamsByCategories = teamService.listByCategory();

        final List<LoopsDone> runnersByCategory = new ArrayList<>();
        teamsByCategories.entrySet().stream().forEach((entry) -> {
            final SortedSet<LoopByRunner> loopsByRunner = new TreeSet<>();

            entry.getValue().stream().forEach((team) -> {
                LOGGER.trace("Looking for loops done by team {}", team);
                final RunnerDto runnerDto = new RunnerDto(team);
                final SortedSet<Loop> teamLoops = loopRepository.findAllForTeam(team.getBib());
                updateRunner(runnerDto, teamLoops);
                LOGGER.trace("{} loops done by team {} so far", teamLoops.size(), team.getBib());

                loopsByRunner.add(new LoopByRunner(runnerDto, teamLoops));
            });

            runnersByCategory.add(new LoopsDone(entry.getKey(), loopsByRunner));
        });

        return runnersByCategory;
    }

    private void updateRunner(final RunnerDto runnerDto, final SortedSet<Loop> loops) {
        LOGGER.trace("Runner {} has done {} loops of {}m", runnerDto.getBib(), loops.size(), runService.getLength());
        runnerDto.setNbLoops(loops.size());
        runnerDto.setDistance(loops.size() * runService.getLength());

        int duration = 0;
        duration = loops.stream().map((loop) -> loop.getDuration()).reduce(duration, Integer::sum);

        LOGGER.trace("Computing speed and rythm for runner {}, with distance {} and duration {}", runnerDto.getBib(), runnerDto.getDistance(), duration);

        runnerDto.setSpeed(computeSpeed(runnerDto.getDistance(), duration));
        runnerDto.setRythm(computeRythm(runnerDto.getDistance(), duration));
    }

    private float computeSpeed(final int distance, final int duration) {
        if (duration == 0) {
            return 0;
        }

        float speed = (float) (((float) distance) * 3.6 / duration);
        LOGGER.trace("Speed ({} * 3.6 / {}) = {}", distance, duration, speed);

        return speed;
    }

    private float computeRythm(final int distance, final int duration) {
        if (distance == 0) {
            return 0;
        }

        float rythm = (float) ((1000 * ((float) duration) / 60) / distance);
        LOGGER.trace("Rythm (1000 * {} / 60) / {} = {}", duration, distance, rythm);

        return rythm;
    }
}
