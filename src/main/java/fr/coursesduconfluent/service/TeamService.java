/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.model.Team;
import fr.coursesduconfluent.model.TeamCategory;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * Manage the teams.
 */
public interface TeamService {

    <S extends Team> S save(final S team);

    <S extends Team> S save(final String name);

    void deleteAll();
    Team getTeamByName(final String name);

    Team getOrCreateTeam(final String name);

    Map<TeamCategory, Set<Team>> listByCategory();
    
    SortedSet<Team> listAll();

    SortedSet<Team> add(String csv);

    SortedSet<Team> overwrite(String csv);
}
