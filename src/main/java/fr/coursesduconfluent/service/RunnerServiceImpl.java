/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.dao.RunnerRepository;
import fr.coursesduconfluent.model.CsvOrder;
import fr.coursesduconfluent.model.Runner;
import fr.coursesduconfluent.model.RunnerCategory;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Runners management service implementation.
 */
@Service
public class RunnerServiceImpl implements RunnerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunnerServiceImpl.class);

    @Autowired
    private final RunnerRepository runnerRepository;

    @Autowired
    public RunnerServiceImpl(final RunnerRepository runnerRepository) {
        this.runnerRepository = runnerRepository;
    }

    @Override
    public SortedSet<Runner> add(final String csv) {
        // Split the CSV to individual rows...based on the dollar character
        final String[] csvRunners = csv.split("(\\$\\$)");
        final SortedSet<Runner> runnersList = new TreeSet<>();

        for (final String runnerRow : csvRunners) {
            final String[] runnerEntries = runnerRow.split(";");

            LOGGER.info("Parsing runner entry: {}", runnerRow);

            // Create and populate a Runner
            final Runner runner = new Runner();

            // Start saving its bib, unique identifier, used to create link with the team amongst others
            final String bib = runnerEntries[CsvOrder.getBibIndex()];
            populateRunner(CsvOrder.BIB, bib, runner);

            for (int i = 0; i < runnerEntries.length; i++) {
                final String token = runnerEntries[i];
                populateRunner(CsvOrder.valueAt(i), token, runner);
            }

            runnersList.add(runner);
        }

        // Save all new entries
        runnerRepository.save(runnersList);

        // Return al entries no matter they are new or not.
        return (SortedSet<Runner>) runnerRepository.findAll();
    }

    @Override
    public SortedSet<Runner> overwrite(final String csv) {
        this.deleteAll();
        return this.add(csv);
    }

    @Override
    public void populateRunner(final CsvOrder type, final String token, final Runner runner) {
        if (token != null && !token.trim().equals("")) {
            LOGGER.debug("Populating runner entry {}, with value {}", type, token);

            switch (type) {
                case BIB:
                    runner.setBib(token);
                    break;
                case LAST_NAME:
                    runner.setLastName(token);
                    break;
                case FIRST_NAME:
                    runner.setFirstName(token);
                    break;
                case EMAIL:
                    runner.setEmail(token);
                    break;
                case SEX:
                    if (token.equals(Runner.WOMAN)) {
                        runner.setIsWoman(true);
                    }
                    break;
                case CATEGORY:
                    runner.setCategory(RunnerCategory.startsWith(token));
                    break;

                default:
                    LOGGER.trace("Skipping data {} for entry {}", token, type);
            }
        }
    }

    @Override
    public void delete(final String runnerBib) {
        runnerRepository.delete(runnerBib);
    }

    @Override
    public void deleteAll() {
        runnerRepository.deleteAll();
    }

    @Override
    public Set<Runner> listAll() {
        final Set<Runner> runners =  (Set<Runner>) runnerRepository.findAll();
        LOGGER.trace("Find all {} runners", runners.size());
        
        return runners;
    }

    @Override
    public Set<Runner> listRunners(final String query) {
        return runnerRepository.findAny(query);
    }

    @Override
    public Map<RunnerCategory, Set<Runner>> listByCategory() {
        LOGGER.debug("Mapping all runners to their category");

        final Map<RunnerCategory, Set<Runner>> mapCategoriesRunners = new HashMap<>(RunnerCategory.values().length);
        SortedSet<Runner> categoryRunners;
        final Set<Runner> handledRunners = new HashSet<>();

        final LocalDate now = LocalDate.now();
        final Set<Runner> allRunners = listAll();

        for (RunnerCategory category : RunnerCategory.values()) {
            LOGGER.trace("Populating runner category {}", category);
            categoryRunners = new TreeSet<>();

            for (final Runner runner : allRunners) {
                LOGGER.trace("Runner {}, category {}", runner.getBib(), runner.getCategory());

                if (!handledRunners.contains(runner) && runner.getCategory() != null && runner.getCategory() == category) {
                    LOGGER.debug("Runner {} added to category {}", runner, category);
                    categoryRunners.add(runner);
                    handledRunners.add(runner);
                }
            }

            LOGGER.info("{} runners in the category {}", categoryRunners.size(), category);
            mapCategoriesRunners.put(category, categoryRunners);
        }

        return mapCategoriesRunners;
    }

    @Override
    public Set<Runner> listRunnersByLastName(final String lastName) {
        return runnerRepository.findByLastName(lastName);
    }

    @Override
    public Set<Runner> listRunnersByFirstName(final String firstName) {
        return runnerRepository.findByFirstName(firstName);
    }

    @Override
    public Set<Runner> listRunnersByEmail(final String email) {
        return runnerRepository.findByEmail(email);
    }

    @Override
    public Runner getRunnerByBib(final String bib) {
        return runnerRepository.findByBib(bib);
    }

    @Override
    public Runner add(final Runner runner) {
        return runnerRepository.save(runner);
    }

    @Override
    public Runner update(final Runner runner) {
        return runnerRepository.save(runner);
    }
}
