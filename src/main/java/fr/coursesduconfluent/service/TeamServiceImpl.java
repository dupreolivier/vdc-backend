/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.dao.TeamRepository;
import fr.coursesduconfluent.model.CsvOrder;
import fr.coursesduconfluent.model.Team;
import fr.coursesduconfluent.model.TeamCategory;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Teams management service implementation.
 */
@Service
public class TeamServiceImpl implements TeamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamServiceImpl.class);

    @Autowired
    private final TeamRepository teamRepository;
    @Autowired
    private final RunnerService runnerService;

    @Autowired
    public TeamServiceImpl(final TeamRepository teamRepository, final RunnerService runnerService) {
        this.teamRepository = teamRepository;
        this.runnerService = runnerService;
    }

    @Override
    public SortedSet<Team> listAll() {
        return (SortedSet<Team>) teamRepository.findAll();
    }

    @Override
    public Team getTeamByName(final String name) {
        return teamRepository.findOne(name);
    }

    @Override
    public Team getOrCreateTeam(final String name) {
        Team team = teamRepository.findOne(name);

        if (team == null) {
            team = new Team();
            team.setName(name);
        }

        return team;
    }

    @Override
    public void deleteAll() {
        teamRepository.deleteAll();
    }

    @Override
    public <S extends Team> S save(final S team) {
        return teamRepository.save(team);
    }

    @Override
    public Team save(final String name) {
        final Team team = new Team();
        team.setName(name);

        return save(team);
    }

    @Override
    public SortedSet<Team> add(final String csv) {
        // Split the CSV to individual rows...based on the dollar character
        final String[] csvTeams = csv.split("(\\$\\$)");
        final SortedSet<Team> teamsList = new TreeSet<>();

        for (final String teamRow : csvTeams) {
            final String[] teamEntries = teamRow.split(";");

            LOGGER.info("Parsing team entry: {}", teamRow);

            // Create and populate a Team
            final Team team = new Team();

            for (int i = 0; i < teamEntries.length; i++) {
                final String token = teamEntries[i];
                runnerService.populateRunner(CsvOrder.valueAt(i), token, team);
                populateTeam(CsvOrder.valueAt(i), token, team);
            }

            teamsList.add(team);
        }

        // Save all new entries
        teamRepository.save(teamsList);

        // Return al entries no matter they are new or not.
        return (SortedSet<Team>) teamRepository.findAll();
    }

    @Override
    public SortedSet<Team> overwrite(final String csv) {
        this.deleteAll();
        return this.add(csv);
    }

    private void populateTeam(final CsvOrder type, final String token, final Team team) {
        if (token != null && !token.trim().equals("")) {
            LOGGER.debug("Populating team entry {}, with value {}", type, token);

            switch (type) {
                case TEAM:
                    team.setName(token);
                    break;
                case TEAM_CATEGORY:
                    team.setTeamCategory(TeamCategory.startsWith(token));
                    break;
                case TEAM_MEMBERS:
                    team.getRunnerNamesSet().addAll(Arrays.asList(token.split(",")));
                    break;

                default:
                    LOGGER.trace("Skipping data {} for entry {}", token, type);
            }
        }
    }

    @Override
    public Map<TeamCategory, Set<Team>> listByCategory() {
        LOGGER.debug("Mapping all teams to their category");

        final Map<TeamCategory, Set<Team>> mapCategoriesTeams = new HashMap<>(TeamCategory.values().length);
        SortedSet<Team> categoryTeams;
        final Set<Team> handledTeams = new HashSet<>();

        final LocalDate now = LocalDate.now();
        final SortedSet<Team> allTeams = listAll();

        for (TeamCategory category : TeamCategory.values()) {
            LOGGER.trace("Populating team category {}", category);
            categoryTeams = new TreeSet<>();

            for (final Team team : allTeams) {
                LOGGER.trace("Team {}, category {}", team.getBib(), team.getTeamCategory());

                if (!handledTeams.contains(team) && team.getTeamCategory() != null && team.getTeamCategory() == category) {
                    LOGGER.debug("Team {} added to team category {}", team, category);
                    categoryTeams.add(team);
                    handledTeams.add(team);
                }
            }

            LOGGER.info("{} teams in the category {}", categoryTeams.size(), category);
            mapCategoriesTeams.put(category, categoryTeams);
        }

        return mapCategoriesTeams;
    }
}
