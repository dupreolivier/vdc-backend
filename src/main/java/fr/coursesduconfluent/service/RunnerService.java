/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.service;

import fr.coursesduconfluent.model.CsvOrder;
import fr.coursesduconfluent.model.Runner;
import fr.coursesduconfluent.model.RunnerCategory;
import java.util.Map;
import java.util.Set;

/**
 * Manage runners.
 */
public interface RunnerService {

    Runner add(final Runner runner);

    Set<Runner> add(String csv);

    Set<Runner> overwrite(String csv);

    void delete(final String runnerBib);

    void deleteAll();

    Runner getRunnerByBib(final String bib);

    Set<Runner> listAll();

    Set<Runner> listRunners(final String query);

    Map<RunnerCategory, Set<Runner>> listByCategory();

    Set<Runner> listRunnersByEmail(final String email);

    Set<Runner> listRunnersByLastName(final String lastName);

    Set<Runner> listRunnersByFirstName(final String firstName);

    Runner update(final Runner runner);

    void populateRunner(final CsvOrder type, final String token, final Runner runner);
}
