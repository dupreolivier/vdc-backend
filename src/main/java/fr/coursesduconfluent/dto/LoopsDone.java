/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dto;

import fr.coursesduconfluent.model.Category;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoopsDone {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopsDone.class);

    private Category category;
    private SortedSet<LoopByRunner> loopsByRunner;

    public LoopsDone() {
    }

    public LoopsDone(final Category category, final SortedSet<LoopByRunner> loopsByRunner) {
        this.category = category;
        this.loopsByRunner = loopsByRunner;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

    public SortedSet<LoopByRunner> getLoopsByRunner() {
        return loopsByRunner;
    }

    public void setLoopsByRunner(final SortedSet<LoopByRunner> loopsByRunner) {
        this.loopsByRunner = loopsByRunner;
    }
}
