/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dto;

import fr.coursesduconfluent.model.Loop;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoopByRunner implements Comparable<LoopByRunner> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopByRunner.class);

    private RunnerDto runner;
    private SortedSet<Loop> loops;

    public LoopByRunner() {
    }

    public LoopByRunner(final RunnerDto runner, final SortedSet<Loop> loops) {
        this.runner = runner;
        this.loops = loops;
    }

    @Override
    public int compareTo(final LoopByRunner loopByRunner) {
        int result = new Integer(loopByRunner.getLoops().size()).compareTo(this.getLoops().size());

        if (result == 0) {
            return loopByRunner.getRunner().compareTo(runner);
        }
        
        return result;
    }

    public SortedSet<Loop> getLoops() {
        return loops;
    }

    public void setLoops(SortedSet<Loop> loops) {
        this.loops = loops;
    }

    public RunnerDto getRunner() {
        return runner;
    }

    public void setRunner(final RunnerDto runner) {
        this.runner = runner;
    }
}
