/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dto;

import fr.coursesduconfluent.model.Runner;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunnerDto  implements Comparable<RunnerDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunnerDto.class);

    private String bib;
    private String lastName, firstName, email, team;
    private boolean isWoman;
    private Date birthDate;
    private int distance, nbLoops;
    private float speed, rythm;

    public RunnerDto() {

    }

    public RunnerDto(final Runner runner) {
        this.birthDate = runner.getBirthDate();
        this.bib = runner.getBib();
        this.email = runner.getEmail();
        this.team = runner.getTeam();
        this.firstName = runner.getFirstName();
        this.lastName = runner.getLastName();
        this.isWoman = runner.isIsWoman();
    }

    @Override
    public int compareTo(final RunnerDto runner) {
        return bib.compareTo(runner.getBib());
    }

    public String getBib() {
        return bib;
    }

    public void setBib(String bib) {
        this.bib = bib;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNbLoops() {
        return nbLoops;
    }

    public void setNbLoops(int nbLoops) {
        this.nbLoops = nbLoops;
    }

    public float getRythm() {
        return rythm;
    }

    public void setRythm(float rythm) {
        this.rythm = rythm;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public boolean isIsWoman() {
        return isWoman;
    }

    public void setIsWoman(boolean isWoman) {
        this.isWoman = isWoman;
    }
}
