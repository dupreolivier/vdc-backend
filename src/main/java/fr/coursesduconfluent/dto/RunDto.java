/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.dto;

import fr.coursesduconfluent.model.Run;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunDto {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunDto.class);

    private ZonedDateTime zonedDateTime;

    private String title;
    private String date;
    private String location;
    private Integer length;
    private Long start;
    private Long end;

    public RunDto() {
    }

    public RunDto(final Run run) {
        title = run.getTitle();
        date = run.getDate().toString();
        length = run.getLength();
        zonedDateTime = run.getStart().atZone(ZoneId.of("Europe/Paris"));
        start = zonedDateTime.toInstant().toEpochMilli();

        if (run.getEnd() != null) {
            zonedDateTime = run.getEnd().atZone(ZoneId.of("Europe/Paris"));
            end = zonedDateTime.toInstant().toEpochMilli();
        }

        location = run.getLocation();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
