/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.controller;

import fr.coursesduconfluent.model.Team;
import fr.coursesduconfluent.service.TeamService;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller to handle teams. <br/>
 * Like any Rest Controller, the goal is to be nothing else than calls to services. Thus, the logic is implemented in the services and can be reused.
 *
 * @see https://spring.io/guides/gs/rest-service-cors/
 */
@CrossOrigin//(origins = "http://localhost:3000")
@RestController
@RequestMapping("/ws/teams")
public class TeamController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamController.class);

    @Autowired
    private TeamService teamService;

    @RequestMapping(method = GET)
    @ResponseBody
    public SortedSet<Team> list() {
        LOGGER.info("Requesting all teams");
        return teamService.listAll();
    }

    @RequestMapping(method = POST)
    @ResponseBody
    public Team add(@RequestBody final Team team) {
        LOGGER.info("Adding team {}", team);
        return teamService.save(team);
    }

    @RequestMapping(method = POST, path = "/add")
    @ResponseBody
    public SortedSet<Team> add(@RequestBody final String csv) {
        LOGGER.info("Adding teams from CSV data {}", csv);
        return teamService.add(csv);
    }

    @RequestMapping(method = POST, path = "/overwrite")
    @ResponseBody
    public SortedSet<Team> overwrite(@RequestBody final String csv) {
        LOGGER.info("Overwriting teams from CSV data {}", csv);
        return teamService.overwrite(csv);
    }

    @RequestMapping(method = PUT)
    @ResponseBody
    public Team update(@RequestBody final Team team) {
        LOGGER.info("Updating team {}", team);
        return teamService.save(team);
    }

    @RequestMapping(method = GET, path = "/{teamName}")
    @ResponseBody
    public Team byName(@RequestBody final String teamName) {
        LOGGER.info("Looking for team by name {}", teamName);
        return teamService.getTeamByName(teamName);
    }

    @RequestMapping(method = DELETE)
    @ResponseBody
    public void deleteAll() {
        LOGGER.info("Deleting all teams ");
        teamService.deleteAll();
    }
}
