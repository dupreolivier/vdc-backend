/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.controller;

import fr.coursesduconfluent.dto.LoopByRunner;
import fr.coursesduconfluent.dto.LoopsDone;
import fr.coursesduconfluent.model.Loop;
import fr.coursesduconfluent.service.LoopService;
import java.text.ParseException;
import java.util.List;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller to handle loops made by runners. <br/>
 * Like any Rest Controller, the goal is to be nothing else than calls to services. Thus, the logic is implemented in the services and can be reused.
 *
 * @see https://spring.io/guides/gs/rest-service-cors/
 */
@CrossOrigin//(origins = "http://localhost:3000")
@RestController
@RequestMapping("/ws/loops")
public class LoopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopController.class);

    @Autowired
    private LoopService loopService;

    @RequestMapping(method = GET)
    @ResponseBody
    public SortedSet<LoopByRunner> list() {
        LOGGER.info("Requesting all loops");
        return loopService.listAll();
    }

    @RequestMapping(method = GET, path = "/{runnerId}")
    @ResponseBody
    public SortedSet<Loop> byRunner(@PathVariable final String runnerId) throws ParseException {
        LOGGER.info("Requesting loops for runner {}", runnerId);
        return loopService.byRunner(runnerId);
    }

    @RequestMapping(method = GET, path = "/teams/{teamId}")
    @ResponseBody
    public SortedSet<Loop> byTeam(@PathVariable final String teamId) throws ParseException {
        LOGGER.info("Requesting loops for team {}", teamId);
        return loopService.byTeam(teamId);
    }

    @RequestMapping(method = GET, path = "/categories")
    @ResponseBody
    public ResponseEntity<List<LoopsDone>> byCategory() {
        LOGGER.info("Requesting loops by categories");
        return new ResponseEntity<>(loopService.byCategory(), HttpStatus.OK);
    }

    @RequestMapping(method = POST, path = "/{bib}")
    @ResponseBody
    public void loop(@PathVariable final String bib) {
        LOGGER.info("Adding one loop to runner {}", bib);
        loopService.loop(bib);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteAll() {
        LOGGER.info("Deleting all loops");
        loopService.deleteAll();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{runnerId}")
    @ResponseBody
    public void deleteLast(@PathVariable final String runnerId) {
        LOGGER.info("Deleting last loop for runner {}", runnerId);
        loopService.deleteLast(runnerId);
    }
}
