/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.controller;

import fr.coursesduconfluent.model.Runner;
import fr.coursesduconfluent.model.RunnerCategory;
import fr.coursesduconfluent.service.RunnerService;
import java.text.ParseException;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller to handle runners. <br/>
 * Like any Rest Controller, the goal is to be nothing else than calls to services. Thus, the logic is implemented in the services and can be reused.
 *
 * @see https://spring.io/guides/gs/rest-service-cors/
 */
@CrossOrigin//(origins = "http://localhost:3000")
@RestController
@RequestMapping("/ws/runners")
public class RunnerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunnerController.class);

    @Autowired
    private RunnerService runnerService;

    @RequestMapping(method = GET)
    @ResponseBody
    public Set<Runner> list() {
        LOGGER.info("Requesting all runners");
        return runnerService.listAll();
    }

    @RequestMapping(method = GET, path = "/search")
    @ResponseBody
    public Set<Runner> list(@RequestParam final String query) {
        LOGGER.info("Looking for runners matching query {}", query);
        return runnerService.listRunners(query);
    }

    @RequestMapping(method = POST)
    @ResponseBody
    public Runner add(@RequestBody final Runner runner) {
        LOGGER.info("Adding runner {}", runner);
        return runnerService.add(runner);
    }

    @RequestMapping(method = POST, path = "/add")
    @ResponseBody
    public Set<Runner> add(@RequestBody final String csv) {
        LOGGER.info("Adding runners from CSV data {}", csv);
        return runnerService.add(csv);
    }

    @RequestMapping(method = POST, path = "/overwrite")
    @ResponseBody
    public Set<Runner> overwrite(@RequestBody final String csv) {
        LOGGER.info("Overwriting runners from CSV data {}", csv);
        return runnerService.overwrite(csv);
    }

    @RequestMapping(method = PUT)
    @ResponseBody
    public Runner update(@RequestBody final Runner runner) {
        LOGGER.info("Updating runner {}, with values {}", runner.getBib(), runner);
        return runnerService.update(runner);
    }

    @RequestMapping(method = DELETE, path = "/{runnerBib}")
    @ResponseBody
    public void delete(@PathVariable final String runnerBib) {
        LOGGER.info("Deleting runner {}", runnerBib);
        runnerService.delete(runnerBib);
    }

    @RequestMapping(method = DELETE)
    @ResponseBody
    public void deleteAll() {
        LOGGER.info("Deleting all runners ");
        runnerService.deleteAll();
    }

    @RequestMapping(method = GET, path = "/{bib}")
    @ResponseBody
    public Runner bib(@PathVariable final String bib) throws ParseException {
        LOGGER.info("Looking for runner with bib {}", bib);
        return runnerService.getRunnerByBib(bib);
    }

    @RequestMapping(method = GET, path = "/email/{email}")
    @ResponseBody
    public Set<Runner> email(@PathVariable final String email) throws ParseException {
        LOGGER.info("Looking for runners with email {}", email);
        return runnerService.listRunnersByEmail(email);
    }

    @RequestMapping(method = GET, path = "/lastName/{lastName}")
    @ResponseBody
    public Set<Runner> lastName(@PathVariable final String lastName) throws ParseException {
        LOGGER.info("Looking for runners with lastName {}", lastName);
        return runnerService.listRunnersByLastName(lastName);
    }

    @RequestMapping(method = GET, path = "/firstName/{firstName}")
    @ResponseBody
    public Set<Runner> firstName(@PathVariable final String firstName) throws ParseException {
        LOGGER.info("Looking for runners with firstName {}", firstName);
        return runnerService.listRunnersByFirstName(firstName);
    }

    @RequestMapping(method = GET, path = "/categories")
    @ResponseBody
    public Map<RunnerCategory, Set<Runner>> categories() throws ParseException {
        LOGGER.info("Mapping runners to their categorie");

        return runnerService.listByCategory();
    }
}
