/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.coursesduconfluent.controller;

import fr.coursesduconfluent.dto.RunDto;
import fr.coursesduconfluent.model.Run;
import fr.coursesduconfluent.service.RunService;
import java.util.SortedSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller to handle loops made by runners. <br/>
 * Like any Rest Controller, the goal is to be nothing else than calls to services. Thus, the logic is implemented in the services and can be reused.
 *
 * @see https://spring.io/guides/gs/rest-service-cors/
 */
@CrossOrigin//(origins = "http://localhost:3000")
@RestController
@RequestMapping("/ws/runs")
public class RunController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunController.class);

    @Autowired
    private RunService runService;

    @RequestMapping(method = POST, path = "/start")
    @ResponseBody
    public RunDto start(@RequestBody final RunDto run) {
        LOGGER.info("Starting the run {}...", run.getTitle());

        return runService.start(run);
    }

    @RequestMapping(method = PUT, path = "/end")
    @ResponseBody
    public RunDto end(@RequestBody final RunDto run) {
        LOGGER.info("Ending the run {}...", run.getTitle());

        return runService.end(run);
    }

    @RequestMapping(method = DELETE)
    @ResponseBody
    public void deleteAll() {
        LOGGER.info("Deleting all runs ");
        runService.deleteAll();
    }
    @RequestMapping(method = GET)
    @ResponseBody
    public SortedSet<Run> list() {
        LOGGER.info("Requesting all runs");
        return runService.listAll();
    }
}
